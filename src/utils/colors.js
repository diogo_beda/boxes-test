export default {
  darkGray: '#474746',
  mediumGray: '#646464',
  lightGray: '#b4b4b4',
  blue: '#3599e6',
  darkBlue: '#1f81cd',
  borderGray: '#e3e3e3',
  green: '#4cc21e',
  darkGreen: '#47b41c'
};

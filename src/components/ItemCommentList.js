import React, { PropTypes } from 'react';
import { StyleSheet, css } from 'aphrodite';
import Immutable from 'immutable';
import moment from 'moment';
import ItemComment from './ItemComment';
import colors from '../utils/colors';

const bySentAt = (comA, comB) => {
  const sentAtA = moment(comA.sent_at, moment.ISO_8601);
  const sentAtB = moment(comB.sent_at, moment.ISO_8601);

  return sentAtA.isBefore(sentAtB) ? -1 : 1;
};

const ItemCommentList = ({ comments }) => {
  return (
    <div className={css(styles.container)}>
      {
        comments
          .sort(bySentAt)
          .map(comment => (
            <ItemComment comment={comment} />
          ))
      }
    </div>
  );
};

ItemCommentList.propTypes = {
  comments: PropTypes.instanceOf(Immutable.List).isRequired
};

const styles = StyleSheet.create({
  container: {
    flex: '1.2',
    padding: '0 10px 0 15px',
    borderTop: `1px solid ${colors.borderGray}`,
    overflowY: 'auto'
  }
});

export default ItemCommentList;

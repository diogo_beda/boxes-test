import React, { PropTypes } from 'react';
import { StyleSheet, css } from 'aphrodite';
import FaHeart from 'react-icons/lib/fa/heart';
import FaRetweet from 'react-icons/lib/fa/retweet';
import FaStar from 'react-icons/lib/fa/star';
import colors from '../utils/colors';

const ItemFooter = ({ item }) => {
  return (
    <div className={css(styles.container)}>
      <div className={css(styles.button)}>
        <FaHeart className={css(styles.icon)} />
        <span className={css(styles.count)}>{item.get('likes_count')}</span>
      </div>
      <div className={css(styles.button)}>
        <FaStar className={css(styles.icon)} />
        <span className={css(styles.count)}>{item.get('wants_count')}</span>
      </div>
      <div className={css(styles.button)}>
        <FaRetweet className={css(styles.icon)} />
        <span className={css(styles.count)}>{item.get('reposts_count')}</span>
      </div>
    </div>
  );
};

ItemFooter.propTypes = {
  item: PropTypes.object.isRequired
};

const styles = StyleSheet.create({
  container: {
    flex: '0.3',
    height: '50px',
    display: 'flex',
    borderTop: `1px solid ${colors.borderGray}`
  },
  button: {
    flex: '1',
    textAlign: 'center',
    color: '#c5c5c5',
    paddingTop: '5px',
    ':hover': {
      background: 'rgba(75,75,75,0.05)'
    }
  },
  icon: {
    fontSize: '22px',
    display: 'block',
    margin: '0 auto'
  },
  count: {
    fontSize: '12px'
  }
});

export default ItemFooter;

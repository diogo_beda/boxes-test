import React, { PropTypes } from 'react';
import { StyleSheet, css } from 'aphrodite';
import ItemDetail from './ItemDetail';
import ItemCommentList from './ItemCommentList';
import ItemCommentInput from './ItemCommentInput';
import ItemFooter from './ItemFooter';

const ItemSidebar = ({ item }) => {
  return (
    <div className={css(styles.container)}>
      <ItemDetail item={item} />
      <ItemCommentList comments={item.get('recent_comments')} />
      <ItemCommentInput />
      <ItemFooter item={item} />
    </div>
  );
};

ItemSidebar.propTypes = {
  item: PropTypes.object.isRequired
};

const styles = StyleSheet.create({
  container: {
    flex: '1',
    display: 'flex',
    flexDirection: 'column'
  }
});

export default ItemSidebar;

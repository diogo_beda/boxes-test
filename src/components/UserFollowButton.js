import React from 'react';
import { StyleSheet, css } from 'aphrodite';
import colors from '../utils/colors';

const UserFollowButton = () => {
  return (
    <button className={css(styles.button)}>Follow</button>
  );
};

const styles = StyleSheet.create({
  button: {
    background: colors.darkGray,
    color: 'white',
    fontSize: '13px',
    height: '28px',
    width: '73px',
    outline: 'none',
    border: 0,
    borderRadius: '3px'
  }
});

export default UserFollowButton;

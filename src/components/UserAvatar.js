import React, { PropTypes } from 'react';
import { StyleSheet, css } from 'aphrodite';
import colors from '../utils/colors';
import UserFollowButton from './UserFollowButton';

const UserAvatar = ({ user, simple }) => {
  return (
    <div className={css(styles.container)}>
      <img
        className={css(styles.image)}
        src={user.getIn(['avatar', 'small_url'])}
        alt={user.get('name')}
      />
      <div className={css(styles.nameContainer)}>
        {
          simple
            ? null
            : <strong className={css(styles.name)}>{user.get('name')}</strong>
        }
        <strong className={css(styles.nickname)}>{user.get('nickname')}</strong>
      </div>
      {
        simple
          ? null
          : <UserFollowButton />
      }
    </div>
  );
};

UserAvatar.propTypes = {
  user: PropTypes.object.isRequired,
  simple: PropTypes.bool
};

export const styles = StyleSheet.create({
  container: {
    display: 'flex'
  },
  image: {
    float: 'left',
    marginRight: '13px',
    width: '37px',
    height: '37px',
    borderRadius: '3px',
    position: 'absolute'
  },
  nameContainer: {
    display: 'flex',
    flexDirection: 'column',
    flex: 3,
    paddingLeft: '50px'
  },
  name: {
    color: colors.blue,
    fontSize: '13px',
    cursor: 'pointer',
    ':hover': {
      color: colors.darkBlue
    }
  },
  nickname: {
    color: colors.darkGray,
    fontSize: '13px',
    cursor: 'pointer'
  }
});

export default UserAvatar;

import React, { PropTypes } from 'react';
import { StyleSheet, css } from 'aphrodite';
import FaShoppingBag from 'react-icons/lib/fa/shopping-bag';
import colors from '../utils/colors';

const ItemLabels = ({ price }) => {
  return (
    <div className={css(styles.container)}>
      <button className={css(styles.button)}>
        <FaShoppingBag className={css(styles.icon)} />
        <span className={css(styles.buttonText)}>Buy</span>
      </button>
      <div className={css(styles.priceTag)}>
        <span className={css(styles.unit)}>$</span>
        <span className={css(styles.price)}>{Math.round(price)}</span>
      </div>
    </div>
  );
}

ItemLabels.propTypes = {
  price: PropTypes.number.isRequired
};

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    position: 'absolute',
    top: '9px',
    right: 0
  },
  button: {
    background: colors.green,
    border: `1px solid ${colors.darkGreen}`,
    color: 'white',
    height: '32px',
    fontSize: '15px',
    marginRight: '8px',
    marginTop: '6px'
  },
  icon: {
    marginTop: '-4px',
    marginRight: '5px'
  },
  buttonText: {
    fontWeight: 100
  },
  priceTag: {
    padding: '0 10px 0 8px',
    height: '44px',
    background: 'rgba(60,60,59,0.9)',
    color: 'white',
    letterSpacing: '-0.04em',
    position: 'relative'
  },
  unit: {
    fontSize: '18px',
    fontWeight: 700,
    display: 'inline-block',
    top: '5px',
    position: 'absolute'
  },
  price: {
    fontSize: '32px',
    paddingLeft: '10px'
  }
});

export default ItemLabels;

import React, { PropTypes } from 'react';
import { StyleSheet, css } from 'aphrodite';
import colors from '../utils/colors';

const ItemCommentInput = () => {
  return (
    <div className={css(styles.container)}>
      <input className={css(styles.input)} type="text" placeholder="Add a comment"/>
    </div>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: '0.23',
    padding: '9px 10px',
    display: 'flex',
    borderTop: `1px solid ${colors.borderGray}`
  },
  input: {
    flex: 1,
    background: '#f2f2f2',
    border: colors.borderGray,
    borderStyle: 'solid',
    borderWidth: '1px',
    height: '36px',
    padding: '0 9px',
    fontSize: '12px',
    borderRadius: '4px',
    textShadow: 'none'
  }
});

export default ItemCommentInput;

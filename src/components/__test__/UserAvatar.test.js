import React from 'react';
import { StyleSheetTestUtils, css } from 'aphrodite';
import Immutable from 'immutable';
import { shallow } from 'enzyme';
import UserAvatar, { styles } from '../UserAvatar';
import UserFollowButton from '../UserFollowButton';

describe('<UserAvatar />', () => {
  beforeEach(() => {
    StyleSheetTestUtils.suppressStyleInjection();
  });

  afterEach(() => {
    StyleSheetTestUtils.clearBufferAndResumeStyleInjection();
  });

  const user = Immutable.Map({
    name: 'Jon',
    nickname: 'jon',
    avatar: {
      small_url: 'url'
    }
  });

  describe('when props.simple is true', () => {
    it('should not render user name', () => {
      const wrapper = shallow(<UserAvatar user={user} simple={true} />);
      expect(wrapper.find(`.${css(styles.name)}`).length).toEqual(0);
    });

    it('should not render the follow button', () => {
      const wrapper = shallow(<UserAvatar user={user} simple={true} />);
      expect(wrapper.find(UserFollowButton).length).toEqual(0);
    });
  });

  describe('when props.simple is false', () => {
    it('should not render user name', () => {
      const wrapper = shallow(<UserAvatar user={user} simple={false} />);
      expect(wrapper.find(`.${css(styles.name)}`).length).toEqual(1);
    });

    it('should not render the follow button', () => {
      const wrapper = shallow(<UserAvatar user={user} simple={false} />);
      expect(wrapper.find(UserFollowButton).length).toEqual(1);
    });
  });
});

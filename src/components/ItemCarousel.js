import React, { PropTypes } from 'react';
import { StyleSheet, css } from 'aphrodite';
import Immutable from 'immutable';
import ItemLabels from './ItemLabels';

const ItemCarousel = ({ photos, price }) => {
  const photo = photos.find(p => p.get('is_main'));
  return (
    <div className={css(styles.container)}>
      <img src={photo.get('large')} className={css(styles.photo)} />
      <ItemLabels price={price} />
    </div>
  );
};

ItemCarousel.propTypes = {
  photos: PropTypes.instanceOf(Immutable.List),
  price: PropTypes.number.isRequired
};

const styles = StyleSheet.create({
  container: {
    flex: '1.5',
    position: 'relative'
  },
  photo: {
    objectFit: 'fill',
    width: '100%',
    height: '100%'
  }
});

export default ItemCarousel;

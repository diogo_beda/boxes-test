import React, { PropTypes } from 'react';
import { StyleSheet, css } from 'aphrodite';
import ItemCarousel from './ItemCarousel';
import ItemSidebar from './ItemSidebar';

const ItemBox = ({ item, loading }) => {
  return (
    <div className={css(styles.container)}>
      <ItemCarousel photos={item.get('item_photos')} price={item.get('price')} />
      <ItemSidebar item={item} />
    </div>
  );
};

ItemBox.propTypes = {
  item: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired
};

const styles = StyleSheet.create({
  container: {
    minWidth: '960px',
    maxWidth: '1004px',
    margin: '40px 0',
    background: 'white',
    display: 'flex',
    borderRadius: '0 5px 5px 0',
    boxShadow: '0 1px 2px 0 rgba(0,0,0,0.2)'
  }
});

export default ItemBox;

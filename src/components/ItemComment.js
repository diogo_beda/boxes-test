import React, { PropTypes } from 'react';
import { StyleSheet, css } from 'aphrodite';
import colors from '../utils/colors';
import UserAvatar from './UserAvatar';

const ItemComment = ({ comment }) => {
  return (
    <div className={css(styles.container)}>
      <UserAvatar user={comment.get('user')} simple={true} />
      <p className={css(styles.comment)}>{comment.get('comment')}</p>
    </div>
  );
};

ItemComment.propTypes = {
  comments: PropTypes.object.isRequired
};

const styles = StyleSheet.create({
  container: {
    margin: '15px 0'
  },
  comment: {
    fontSize: '13px',
    fontWeight: 100,
    color: colors.mediumGray,
    margin: 0,
    paddingLeft: '50px'
  }
});

export default ItemComment;

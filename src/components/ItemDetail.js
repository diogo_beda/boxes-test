import React, { PropTypes } from 'react';
import { StyleSheet, css } from 'aphrodite';
import LineClamp from 'shiitake';
import FaMapMarker from 'react-icons/lib/fa/map-marker';
import UserAvatar from './UserAvatar';
import colors from '../utils/colors';

const ItemDetail = ({ item }) => {
  return (
    <div className={css(styles.container)}>
      <LineClamp
        lines={3}
        className={css(styles.name)}
        tagName="h2"
      >
        {item.get('name')}
      </LineClamp>
      <span className={css(styles.location)}>
        <FaMapMarker className={css(styles.locationIcon)} />
        <span>{item.get('location_details')}</span>
      </span>
      <div className={css(styles.infoContainer)}>
        <UserAvatar user={item.get('user')} />
        <p className={css(styles.description)}>{item.get('description')}</p>
      </div>
    </div>
  );
};

ItemDetail.propTypes = {
  item: PropTypes.object.isRequired
};

const styles = StyleSheet.create({
  container: {
    flex: '1',
    padding: '0 10px 0 15px'
  },
  name: {
    fontSize: '31px',
    fontFamily: '"Roboto", sans-serif',
    color: colors.darkGray,
    margin: '16px 0 5px',
    paddingBottom: '3px',
    lineHeight: 1.05
  },
  location: {
    color: colors.lightGray,
    fontSize: '13px',
    cursor: 'pointer',
    ':hover': {
      textDecoration: 'underline'
    }
  },
  locationIcon: {
    marginTop: '-3px',
    fontSize: '16px'
  },
  infoContainer: {
    padding: '13px 0'
  },
  description: {
    fontSize: '13px',
    fontWeight: 100,
    margin: 0,
    color: colors.mediumGray,
    paddingLeft: '50px'
  }
});

export default ItemDetail;

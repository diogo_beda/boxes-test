import React, { Component } from 'react';
import { StyleSheet, css } from 'aphrodite';
import { connect } from 'react-redux';
import { fetchItem } from '../redux/modules/item';
import ItemBox from '../components/ItemBox';

class App extends Component {
  // componentDidMount() {
  //   this.props.fetchItem(278878, 47776);
  // }

  render() {
    const { item, loading } = this.props;

    return (
      <div className={css(styles.container)}>
        <ItemBox item={item} loading={loading} />
      </div>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: '100vh',
    width: '100vw',
    background: '#f5f5f1',
    display: 'flex',
    justifyContent: 'center',
    lineHeight: 1.4
  }
});

const mapStateToProps = ({ item }) => {
  return {
    loading: item.get('loading'),
    item: item.get('item')
  };
};
export default connect(
  mapStateToProps,
  { fetchItem }
)(App);

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import Immutable from 'immutable';
import createStore from './redux';
import App from './containers/App';
import itemJson from './item.json';
import './index.css';

const store = createStore({
  item: Immutable.fromJS({
    loading: false,
    item: itemJson.item
  })
});
ReactDOM.render((
  <Provider store={store}>
    <App />
  </Provider>
), document.getElementById('root'));

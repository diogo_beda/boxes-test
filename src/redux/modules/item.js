import Immutable from 'immutable';

export const ITEM_REQUEST = 'ITEM_REQUEST';
export const ITEM_SUCCESS = 'ITEM_SUCCESS';
export const ITEM_FAILURE = 'ITEM_FAILURE';

export const initialState = Immutable.Map({
  loading: false,
  item: {}
});
export default (state = initialState, action) => {
  switch (action.type) {
    case ITEM_REQUEST:
      return state.set('loading', true);
    case ITEM_SUCCESS:
      return state.merge({
        loading: false,
        item: action.response
      });
    case ITEM_FAILURE:
      return state.merge({
        loading: false,
        error: action.error
      });
    default:
      return state;
  }
};

export function fetchItem(itemId, userId) {
  return {
    apiCall: {
      url: `/users/${userId}/items/${itemId}`,
      method: 'get',
      actionTypes: [ITEM_REQUEST, ITEM_SUCCESS, ITEM_FAILURE]
    }
  };
}

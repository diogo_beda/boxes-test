import reducer, {
  fetchItem,
  initialState,
  ITEM_REQUEST,
  ITEM_SUCCESS,
  ITEM_FAILURE
} from '../item';

describe('Item reducer', () => {
  describe('on initialization', () => {
    const state = undefined;
    const action = { type: '@@redux/INIT' };

    it('should return initialState', () => {
      expect(reducer(state, action)).toEqual(initialState);
    });
  });

  describe('on ITEM_REQUEST', () => {
    const state = initialState;
    const action = { type: ITEM_REQUEST };

    it('should set loading to true', () => {
      expect(reducer(state, action).toJS()).toEqual({ loading: true, item: {} });
    });
  });

  describe('on ITEM_SUCCESS', () => {
    const state = initialState.set('loading', true);
    const action = { type: ITEM_SUCCESS, response: { id: 1 } };

    it('should set loading to true', () => {
      expect(reducer(state, action).toJS()).toEqual({ loading: false, item: { id: 1 } });
    });
  });

  describe('on ITEM_SUCCESS', () => {
    const state = initialState.set('loading', true);
    const action = { type: ITEM_FAILURE, error: { message: 'Not found' } };

    it('should set loading to true', () => {
      expect(reducer(state, action).toJS()).toEqual({
        loading: false,
        item: {},
        error: { message: 'Not found' }
      });
    });
  });
});

describe('fetchItem action creator', () => {
  it('should return the right action', () => {
    expect(fetchItem(1, 2)).toEqual({
      apiCall: {
        url: '/users/2/items/1',
        method: 'get',
        actionTypes: [ITEM_REQUEST, ITEM_SUCCESS, ITEM_FAILURE]
      }
    });
  });
});

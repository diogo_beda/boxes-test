import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.box.es/api',
  headers: {
    'Accept': 'application/json'
  }
});

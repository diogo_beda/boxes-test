import apiMiddleware from '../api';

describe('API Middleware', () => {
  let next = null;
  let action = null;
  let apiClient = null;

  beforeEach(() => {
    next = jest.fn();
    apiClient = {
      request: () => new Promise(resolve => resolve())
    };
  });

  describe('when action does not have apiCall key', () => {
    beforeEach(() => {
      action = { type: 'MY_ACTION' };
    });

    it('should dispatch given action', () => {
      apiMiddleware(apiClient)()(next)(action);
      expect(next).lastCalledWith(action);
    });
  });

  describe('when action has apiCall key', () => {
    const warnLog = 'To use api middleware your action must have a method, path and actionTypes'

    describe('when apiCall.url is nil', () => {
      beforeEach(() => {
        action = {
          apiCall: { method: 'get' }
        };
        console.warn = jest.fn();
      });

      it('should dispatch given action', () => {
        apiMiddleware(apiClient)()(next)(action);
        expect(console.warn).lastCalledWith(warnLog);
        expect(next).lastCalledWith(action);
      });
    });

    describe('when apiCall.method is nil', () => {
      beforeEach(() => {
        action = {
          apiCall: { url: '/item' }
        };
        console.warn = jest.fn();
      });

      it('should dispatch given action', () => {
        apiMiddleware(apiClient)()(next)(action);
        expect(console.warn).lastCalledWith(warnLog);
        expect(next).lastCalledWith(action);
      });
    });

    describe('when apiCall.actionTypes is not an array', () => {
      beforeEach(() => {
        action = {
          apiCall: {
            url: '/item',
            method: 'get'
          }
        };
        console.warn = jest.fn();
      });

      it('should dispatch given action', () => {
        apiMiddleware(apiClient)()(next)(action);
        expect(console.warn).lastCalledWith(warnLog);
        expect(next).lastCalledWith(action);
      });
    });

    describe('when apiCall object is valid', () => {
      beforeEach(() => {
        action = {
          apiCall: {
            url: '/item',
            method: 'get',
            actionTypes: ['ITEM_REQUEST', 'ITEM_SUCCESS', 'ITEM_FAILURE']
          }
        };
      });

      it('should dispatch REQUEST action', () => {
        apiMiddleware(apiClient)()(next)(action);
        expect(next).lastCalledWith({ type: 'ITEM_REQUEST' });
      });

      describe('when request is successful', () => {
        beforeEach(() => {
          apiClient = {
            request: () => new Promise(resolve => resolve({ id: 1 }))
          };
        });


        it('should dispatch a SUCCESS action', (done) => {
          apiMiddleware(apiClient)()(next)(action)
            .then(() => {
              expect(next).lastCalledWith({
                type: 'ITEM_SUCCESS',
                response: { id: 1 }
              });
              done();
            });
        });
      });

      describe('when request has failed', () => {
        beforeEach(() => {
          apiClient = {
            request: () => new Promise((resolve, reject) => reject({ message: 'Not found' }))
          };
        });

        it('should dispatch a SUCCESS action', (done) => {
          apiMiddleware(apiClient)()(next)(action)
            .then(() => {
              expect(next).lastCalledWith({
                type: 'ITEM_FAILURE',
                error: { message: 'Not found' }
              });
              done();
            });
        });
      });
    });
  });
});

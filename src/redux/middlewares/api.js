import { isArrayLike } from 'ramda';

export default apiClient => store => next => action => {
  if (!action.apiCall) {
    return next(action);
  }

  const { url, method, data, headers, actionTypes } = action.apiCall;

  if (!url || !method || !isArrayLike(actionTypes)) {
    console.warn('To use api middleware your action must have a method, path and actionTypes');
    return next(action);
  }
  const [REQUEST, SUCCESS, FAILURE] = actionTypes;

  next({ type: REQUEST });
  return apiClient.request({ url, method, data, headers })
    .then((response) => next({ type: SUCCESS, response }))
    .catch((error) => next({ type: FAILURE, error }));
};

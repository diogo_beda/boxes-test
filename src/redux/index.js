import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import apiMiddleware from './middlewares/api';
import apiClient from './utils/apiClient';
import rootReducer from './rootReducer';

export default (initialState) => {
  return createStore(rootReducer, initialState, composeWithDevTools(
    applyMiddleware(apiMiddleware(apiClient))
  ));
};
